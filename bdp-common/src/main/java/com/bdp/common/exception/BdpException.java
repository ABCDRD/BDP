package com.bdp.common.exception;

/**
 * BDP异常基类
 *
 * @author Ken.Zheng
 * @date 2022/10/17
 **/
public class BdpException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * 错误提示
     */
    private String message;

    /**
     * 空构造方法，避免反序列化问题
     */
    public BdpException() {
    }

    public BdpException(String message) {
        this.message = message;
    }

    public BdpException(Throwable e) {
        super(e);
    }

    public BdpException(Throwable e, String message) {
        super(e);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public BdpException setMessage(String message) {
        this.message = message;
        return this;
    }
}
