package com.bdp.common.enums;

/**
 * 文件类型枚举
 *
 * @author Ken.Zheng
 * @date 2022/10/17
 */
public enum Filetype {
    GZ("gz"), TARGZ("tar.gz"), TGZ("tgz"), RPM("rpm"),
    ;
    private final String type;

    private Filetype(String type) {
        this.type = type;
    }

    public String getType(){
        return type;
    }
}
