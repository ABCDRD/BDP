package com.bdp.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

/**
 * Linux优化工具类
 *
 * @author Ken.Zheng
 * @date 2022/10/23
 **/
public class LinuxOptimizeUtils {

    private static Logger LOG = LoggerFactory.getLogger(LinuxOptimizeUtils.class);

    private static List<String> limitsList = Arrays.asList("* soft nofile ", "* hard nofile ", "* soft nproc ", "* hard nproc ");

    private static String defalutLimitConf = "/etc/security/limits.conf";

    //删除行： sed -i '/* soft nofile /d' /etc/security/limits.conf
    private static String replaceTemplate = "sed -i '/%s/d' %s";

    //增加配置：echo "* soft nofile `cat /proc/sys/fs/nr_open`" >> /etc/security/limits.conf
    //增加配置2： echo "* soft nofile 1048576" >> /etc/security/limits.conf  `
    private static String addTemplate = "echo \"%s 65535\" >> %s";

    private static String grep4Template = "grep '%s\\|%s\\|%s\\|%s' %s ";

    private static final int defaultLimit = 65535;

    /**
     * 设置服务器的limits配置信息.
     *
     * @return 返回设置状态。
     */
    public static boolean updateLimits(SshUtils ssh) {
        LOG.debug("设置服务器limits打开文件数");
        //查询现有配置情况 ，日志输出
        String grepCmd = String.format(grep4Template, limitsList.get(0), limitsList.get(1), limitsList.get(2), limitsList.get(3), defalutLimitConf);
        ssh.execCmd(grepCmd);
        for (String config : limitsList) {
            String replaceCmd = String.format(replaceTemplate, config, defalutLimitConf);
            ssh.execCmd(replaceCmd);
        }
        for (String config : limitsList) {
            String addCmd = String.format(addTemplate, config, defalutLimitConf);
            ssh.execCmd(addCmd);
        }

        grepCmd = String.format(grep4Template, limitsList.get(0), limitsList.get(1), limitsList.get(2), limitsList.get(3), defalutLimitConf);
        ssh.execCmd(grepCmd);

        //生效
        ssh.execCmd("sysctl -a");
        //查询
        ssh.execCmd("ulimit -n");

        return true;
    }

}
