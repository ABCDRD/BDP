package com.bdp.common.utils;

import com.bdp.common.enums.Filetype;
import com.bdp.common.exception.BdpException;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Tar文件工具包
 *
 * @author Ken.Zheng
 * @date 2022/10/17
 **/
public class ArchiveUitls {

    private static Logger LOG = LoggerFactory.getLogger(ArchiveUitls.class);

    /**
     * 解压gz类型文件，如gz,tar.gz,tgz.
     * {@link ArchiveUitls#unCompressToDir(File, File, Filetype)}
     */
    public static void unCompressGzToDir(String sourceCompressFile, String targetDir) {
        unCompressToDir(new File(sourceCompressFile), new File(targetDir), Filetype.GZ);
    }

    /**
     * {@link ArchiveUitls#unCompressToDir(File, File, Filetype)}
     */
    public static void unCompressToDir(String sourceCompressFile, String targetDir, Filetype type) {
        unCompressToDir(new File(sourceCompressFile), new File(targetDir), type);
    }

    /**
     * 将{@code sourceTarGzFile} 解压到{@code targetDir}  目录,会覆盖/重置目录中原文件
     *
     * @param sourceCompressFile 源gz文件
     * @param targetDir          目标目录
     * @param type               文件类型 {@link  Filetype}
     * @throws BdpException 源文件不合法，或目标目录不合法时抛出,IOException也会转到此异常
     */
    public static void unCompressToDir(File sourceCompressFile, File targetDir, Filetype type) {

        LOG.debug("gz source:{},targetDir:{}", sourceCompressFile, targetDir);
        if (!sourceCompressFile.exists()) {
            throw new BdpException(String.format("请检查{}文件是否存在", sourceCompressFile));
        }
        boolean gzType = type.equals(Filetype.GZ) || type.equals(Filetype.TARGZ) || type.equals(Filetype.TGZ);
        if (!gzType) {
            throw new BdpException(String.format("当前仅支持{}，{}，{}结尾文件，{}", Filetype.GZ, Filetype.TARGZ, Filetype.TGZ, sourceCompressFile.getName()));
        }
        if (!targetDir.exists()) {
            boolean flag = targetDir.mkdirs();
            if (!flag) {
                throw new BdpException(String.format("目标目录{}不存在，且无法创建", targetDir));
            }
        }
        try {// decompressing *.tar.gz files to tar
            TarArchiveInputStream tarArchiveInputStream = null;
            if (gzType) {
                tarArchiveInputStream = new TarArchiveInputStream(new GzipCompressorInputStream(Files.newInputStream(sourceCompressFile.toPath())));
            }
            TarArchiveEntry entry;
            // 将 tar 文件解压到 targetDir 目录下
            // 将 tar.gz文件解压成tar包,然后读取tar包里的文件元组，复制文件到指定目录
            while ((entry = tarArchiveInputStream.getNextTarEntry()) != null) {
                if (entry.isDirectory()) {
                    continue;
                }
                File targetFile = new File(targetDir, entry.getName());
                LOG.debug("ungz source:{},targetDir:{}", entry.getName(), targetFile);
                File parent = targetFile.getParentFile();
                if (!parent.exists()) {
                    parent.mkdirs();
                }
                // 将文件写出到解压的目录
                long fileSize = IOUtils.copy(tarArchiveInputStream, Files.newOutputStream(targetFile.toPath()));
                LOG.debug("ungz source:{},targetDir:{} Done,Size:{},{}", entry.getName(), targetFile, fileSize,FileUtils.byteCountToDisplaySize(fileSize));
            }
            LOG.info("gz Done source:{},targetDir:{}", sourceCompressFile, targetDir);
        } catch (IOException e) {
            throw new BdpException(e, String.format("gz 解压异常 source:{},targetDir:{}", sourceCompressFile, targetDir));
        }
    }

//    /**
//     * 解压流去解析，解压之后到一个新路径，返回一个解压文件的集合
//     * @param fileName
//     * @param unZipPath
//     * @return
//     */
//    public Set<String> unGzip(String fileName, String unZipPath) {
//        Set<String> set = new HashSet<>();
//        try {
//            InputStream inputStream = ftpHelper.getInputStream(fileName);
//            TarArchiveInputStream fin = new TarArchiveInputStream(new GzipCompressorInputStream(inputStream));
//            System.out.println("********开始执行gzip");
//            TarArchiveEntry entry;
//            // 将 tar 文件解压到 extractPath 目录下
//            while ((entry = fin.getNextTarEntry()) != null) {
//                if (entry.isDirectory()) {
//                    continue;
//                }
//                //目标文件位置
//                File curfile = new File(unZipPath + entry.getName());
//                File parent = curfile.getParentFile();
//                System.out.println("***文件保存的路径" + curfile.getAbsolutePath());
//                if (!parent.exists()) {
//                    parent.mkdirs();
//                }
//                // 将文件写出到解压的目录
//                IOUtils.copy(fin, new FileOutputStream(curfile));
//                set.add(curfile.getAbsolutePath());
//            }
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return set;
//    }

//    // 解压tar包
//    public Set<String> unTar(String fileName, String unZipPath) {
//        Set<String> set = new HashSet<>();
//        try {
//            InputStream inputStream = ftpHelper.getInputStream(fileName);
//
//            TarArchiveInputStream fin = new TarArchiveInputStream(inputStream);
//            System.out.println("********开始执行tar");
//            // File extraceFolder = new File(unZipPath);
//            TarArchiveEntry entry;
//
//            // 将 tar 文件解压到 extractPath 目录下
//            while ((entry = fin.getNextTarEntry()) != null) {
//                if (entry.isDirectory()) {
//                    continue;
//                }
//                System.out.println("*********创建目录开始" + unZipPath);
//                boolean flag = ftpHelper.isDirExistCreate(unZipPath);
//                System.out.println("***********解压目录创建结束" + flag);
//                System.out.println("***文件名称" + entry.getName());
//                File curfile = new File(unZipPath + entry.getName());
//                File parent = curfile.getParentFile();
//                System.out.println("***文件保存的路径" + curfile.getAbsolutePath());
//                if (!parent.exists()) {
//                    parent.mkdirs();
//                }
//                set.add(curfile.getAbsolutePath());
//                // 将文件写出到解压的目录
//                IOUtils.copy(fin, new FileOutputStream(curfile));
//            }
//
//            //fin.close();
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return set;
//    }
}
