package com.bdp.common.utils;

import com.jcraft.jsch.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;

/**
 * SSH工具类
 * https://www.cnblogs.com/goloving/p/15023195.html
 * https://www.cnblogs.com/yang-manong/p/jsch.html
 *
 * @author Ken.Zheng
 * @date 2022/10/17
 **/
public class SshUtils {

    private static Logger LOG = LoggerFactory.getLogger(ArchiveUitls.class);

    private static final String ENCODE_UTF_8 = "UTF-8";

    private Date lastPushDate = null;

    private Session sshSession;

    private ChannelSftp channel;

    public SshUtils(String host, int port, String username, String password) throws Exception {
        connection(host, port, username, password);
    }

    /**
     * 链接服务器
     */
    public void connection(String host, int port, String username, String password) throws Exception {
        JSch jsch = new JSch();
        jsch.getSession(username, host, port);
        //根据用户名，密码，端口号获取session
        sshSession = jsch.getSession(username, host, port);
        sshSession.setPassword(password);
        //修改服务器/etc/ssh/sshd_config 中 GSSAPIAuthentication的值yes为no，解决用户不能远程登录
        sshSession.setConfig("userauth.gssapi-with-mic", "no");

        //为session对象设置properties,第一次访问服务器时不用输入yes
        sshSession.setConfig("StrictHostKeyChecking", "no");
        sshSession.connect();
        //获取sftp通道
        channel = (ChannelSftp) sshSession.openChannel("sftp");
        channel.connect();
        LOG.info("ssh {} 连接sftp成功!", host);
    }

    /**
     * 关闭通道
     */
    public void closeChannel() {
        if (null != channel) {
            try {
                channel.disconnect();
            } catch (Exception e) {
                LOG.error("关闭SFTP通道发生异常:", e);
            }
        }
        if (null != sshSession) {
            try {
                sshSession.disconnect();
            } catch (Exception e) {
                LOG.error("SFTP关闭 session异常:", e);
            }
        }
    }

    /**
     * 判断目录是否存在
     *
     * @param directory 路径
     */
    public boolean existDir(String directory) {
        boolean isDirExistFlag = false;
        try {
            SftpATTRS sftpATTRS = channel.lstat(directory);
            isDirExistFlag = true;
            return sftpATTRS.isDir();
        } catch (Exception e) {
            if ("no such file".equals(e.getMessage())) {
                isDirExistFlag = false;
            }
        }
        return isDirExistFlag;
    }

    /**
     * 进入该路径
     */
    public void cdDir(String directory) throws SftpException {
        String[] directories = directory.split("/");
        for (int i = 0; i < directories.length; i++) {
            String d = directories[i];
            if (StringUtils.isBlank(d)) {
                continue;
            }

            try {
                if (i == 1) {
                    channel.cd("/" + d);
                    continue;
                }

                channel.cd(d);

            } catch (Exception e) {

                channel.mkdir(d);
                channel.cd(d);
            }
        }

    }


    /**
     * 获取文件夹下的文件
     *
     * @param directory 路径
     */
    public Vector<?> listDir(String directory) {
        try {
            if (existDir(directory)) {
                Vector<?> vector = channel.ls(directory);
                //移除上级目录和根目录："." ".."
                vector.remove(0);
                vector.remove(0);
                return vector;
            }
        } catch (SftpException e) {
            LOG.error("获取文件夹信息异常！", e);
        }
        return null;
    }

    /**
     * 列出本目录所有的文件和子目录
     */
    public Set<String> listAllDir(String directory) {

        Set<String> set = new HashSet<>();

        Vector ls = null;
        try {
            ls = channel.ls(directory);
        } catch (SftpException e) {
            LOG.error("the {} is not directory!!", directory);
        }

        if (ls == null) {
            return null;
        }

        Iterator iterator = ls.iterator();

        while (iterator.hasNext()) {
            ChannelSftp.LsEntry next = (ChannelSftp.LsEntry) iterator.next();
            String filename = next.getFilename();

            if (filename.startsWith(".")) {
                continue;
            }
            set.add(filename);

        }

        return set;
    }

    /**
     * 获取本地某路径下文件
     */
    public List<File> getFiles(String realpath) {
        List<File> files = new ArrayList<>();
        File realFile = new File(realpath);
        if (realFile.isDirectory()) {
            File[] subfields = realFile.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    if (null == lastPushDate) {
                        return true;
                    } else {
                        long modifyDate = file.lastModified();
                        return modifyDate > lastPushDate.getTime();
                    }
                }
            });

            if (subfields.length == 0) {
                return files;
            }

            for (File file : subfields) {
                if (file.isDirectory()) {
                    List<File> filesTmp = getFiles(file.getAbsolutePath());
                    if (filesTmp != null && filesTmp.size() > 0) {
                        files.addAll(filesTmp);
                    }
                } else {
                    files.add(file);
                }

            }
        } else {
            files.add(realFile);
        }
        return files;
    }

    /**
     * 删除文件
     */
    public void deleteFile(String directory) {
        String tryStartUpShCmd2 = "rm -rf  " + directory;
        execCmd(tryStartUpShCmd2);
    }

    /**
     * exec 执行ssh命令
     */
    public String execCmd(String command) {
        LOG.debug("ssh执行:{}", command);
        //1.默认方式，执行单句命令
        InputStream in;
        String result = null;
        try {
            ChannelExec channelExec = (ChannelExec) sshSession.openChannel("exec");
            in = channelExec.getInputStream();
            channelExec.setCommand(command);
            channelExec.setErrStream(System.err);
            channelExec.connect();
            result = IOUtils.toString(in, ENCODE_UTF_8);

            channelExec.disconnect();

        } catch (Exception e) {
            LOG.error(e.getMessage());
        }
        LOG.debug("ssh执行:{},结果:{}", command, result);
        return result;

    }

    /**
     * 上传到ssh服务器.
     *
     * @param in               待上传的本地文件流
     * @param distFileFullPath 服务器上文件的绝对地址.需要自行创建目录.
     * @return 上传状态.
     */
    public boolean uploadFile(InputStream in, String distFileFullPath) {
        try {
            channel.put(in, distFileFullPath);
            in.close();
            return true;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 下载文件
     *
     * @param serverFolder 服务器目录
     * @param filename     服务器上待下载文件名
     * @param distFilePath 本地目标端文件夹
     * @param distFileName 本地下载的文件命名
     * @return 下载状态.
     */
    public boolean downloadFile(String serverFolder, String filename, String distFilePath, String distFileName) {
        try {
            if (StringUtils.isEmpty(distFileName)) {
                distFileName = filename;
            }
            channel.cd(serverFolder);
            OutputStream out = new FileOutputStream(new File(distFilePath, distFileName));
            channel.get(filename, out);
            out.flush();
            out.close();
            return true;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
