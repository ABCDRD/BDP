package com.bdp.common.utils;

import com.jcraft.jsch.SftpException;
import org.apache.commons.io.FileUtils;
import org.junit.*;
import org.junit.runners.MethodSorters;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Tar文件工具包
 *
 * @author Ken.Zheng
 * @date 2022/10/17
 **/
@FixMethodOrder(value = MethodSorters.NAME_ASCENDING)
public class TestSshUitls {

    private static String sshHost = "192.168.1.5";
    private static int sshPort = 22;
    private static String sshUserName = "root";
    private static String sshPassword = "Ken123";

    private static SshUtils ssh;

    @BeforeClass
    public static void before() {
        try {
            ssh = new SshUtils(sshHost, sshPort, sshUserName, sshPassword);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void test01Exist() {
        Assert.assertEquals(ssh.existDir("/root/"), true);
        Assert.assertEquals(ssh.existDir("/root/asdf/dfdfa"), false);
    }

    @Test
    public void test02Cd() {
        try {
            ssh.cdDir("/root/");
        } catch (SftpException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void test03List() {
        ssh.listDir("/root/");
    }

    @Test
    public void test04List() {
        ssh.listAllDir("/root/");
    }

    @Test
    public void test05MkDir() {
        String pwd = ssh.execCmd("pwd");
        System.out.println(pwd);
        ssh.execCmd("mkdir /root/temp-ssh");
    }

    @Test
    public void test06Mv() {
        ssh.execCmd("mv /root/temp-ssh  /root/tmp-ssh2");
    }

    @Test
    public void test07CreateFile() {
        ssh.execCmd("touch /root/tmp-ssh2/a.txt && echo 123 >> /root/tmp-ssh2/a.txt");
    }

    @Test
    public void test07ListFile() {
        List<File> allFiles = ssh.getFiles("/root/tmp-ssh2");
        System.out.println(Arrays.asList(allFiles));
    }

    @Test
    public void test08Upload() {
        try {
            File f = new File("D:/abc.txt");
            f.createNewFile();
            List<String> lines = new ArrayList<>();
            lines.add("abc");
            lines.add("abc123");
            FileUtils.writeLines(f, lines);

            boolean flag = ssh.uploadFile(new FileInputStream(f), "/root/abc1.txt");
            Assert.assertTrue(flag);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void test09Archive() {
        boolean flag = ssh.downloadFile("/root/", "abc1.txt", "D:/", "abc.txt");
        Assert.assertTrue(flag);
        File f = new File("D:/abc.txt");
        flag = f.delete();
        Assert.assertTrue(flag);
    }

    @Test
    public void test10ExeAndLog() {
        String logs = ssh.execCmd("cat /etc/hosts");
        System.out.println("cat /etc/hosts 结果:" + logs);
    }

    @Test
    public void test10Del() {
        ssh.deleteFile("/root/tmp-ssh2");
    }


    @Test
    public void test11UpdateLimits() {
        LinuxOptimizeUtils.updateLimits(ssh);
    }

    @AfterClass
    public static void after() {
        try {
            ssh.closeChannel();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
