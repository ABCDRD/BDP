package com.bdp.common.utils;

import com.bdp.common.exception.BdpException;
import org.junit.Test;

/**
 * Tar文件工具包
 *
 * @author Ken.Zheng
 * @date 2022/10/17
 **/
public class TestArchiveUitls {

    @Test
    public void testUntar() {
        ArchiveUitls.unCompressGzToDir("F:\\back\\软件\\BD\\flink-1.13.1-bin-scala_2.12.tgz", "D:/tmp/a/");
    }

    @Test

    public void testExpect() {
        ArchiveUitls.unCompressGzToDir("F:\\back\\软件\\BD\\flink-1.13.1-bin-scala_2.12.tgz", "Z:/tmp/a/");
    }

}
