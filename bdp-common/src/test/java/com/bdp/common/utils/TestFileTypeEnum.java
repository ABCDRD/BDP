package com.bdp.common.utils;

import com.bdp.common.enums.Filetype;
import org.junit.Assert;
import org.junit.Test;

/**
 * 测试文件类型枚举
 *
 * @author Ken.Zheng
 * @date 2022/10/17
 **/
public class TestFileTypeEnum {

    @Test
    public void testFileTypeEnum() {
        Assert.assertEquals(Filetype.GZ.getType(), "gz");
        Assert.assertEquals(Filetype.TARGZ.getType(), "tar.gz");
    }
}
