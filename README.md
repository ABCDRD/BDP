
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">BDP v1.0.0</h1>
<h4 align="center">开源易用的大数据平台</h4>
<p align="center">
	<a href="https://gitee.com/ABCDRD/BDP/blob/dev-bigdata/LICENSE"><img src="https://img.shields.io/github/license/mashape/apistatus.svg"></a>
</p>

## 背景

很多实力较强的大数据公司均会基于Ambari或自研一些优秀的大数据平台，但是均不开源；个人或小企业只能使用命令人肉部署、运维，重复且低效；偶有使用CDP到Ambari这些套件，简化了一些部署，但是实际用起来坑依然很多（如ARM适配，扩展组件，安装包巨大，怎么上云？），大家都要趟一遍；hortonworks并入cloudera，Ambari项目更新并不活跃。恰好近期又要整下hadoop+yarn了，所以想利用空闲休息时间开始自己写了一套大数据平台。希望能减轻大数据相关的工作量。

## BDP

工程取名 BDP，Big Data Platform，大数据平台，旨在为各类基础大数据组件提供初始部署、配置、监控、运维、告警及对外提供API等，希望能减轻大数据运维工作量。

## 目标

目标：开源易用的大数据平台

通过提供各类大数据组件能力：Hadoop, Spark，Flink, Hive, HBase, Presto 等，兼容开源生态，支持安全管控，提供一站式运维、管理平台，帮助个人和企业快速构建自己的大数据平台。

## 当前功能

1.  系统管理：基于RuoYi的用户、角色、权限、日志等系统管理功能及基础框架
2.  

## 规划功能

1. 部署：增加单节点Hadoop+Yarn；
2. 部署：各组件分布式部署；
3. 监控：增加各组件监控、告警
4. 安全：增加各组件的安全策略及组件，修复/处理大数据常见漏洞；
5. 性能：对常见性能项目进行优化；
6. API：将平台能力API化；
7. 智能：扩展运维：将常见的问题进行自动修复处理；
8. 部署：Hive+Hbase+Presto+Flink+Spark+Kafka，快速搭建开发环境；
9. 上云：上K8S，服务化；

## 近期工作

### TODO LIST

* 9.动态修改xml中参数并重启生效
* 8.远程检查hadoop状态并自动重启
* 7.远程自检hadoop dfs等命令
* 6.远程部署hadoop并启动
* 5.远程检查jdk及部署
* 4.远程检查服务器配置并初始化

### DONE LIST

* 3.远程上传文件、复制、执行命令、获取日志工具类 22.10.23
* 2.ssh 工具类 22.10.23
* 1.解压tar等工具类  22.10.17

## 开源依赖

基于开源，也回馈开源，工程毫无保留给个人及企业免费使用。

* https://onlineconvertfree.com/  制作ico
* https://pixso.cn/  站酷小薇LOGO体 制作Logo BDP
* [RuoYi](https://gitee.com/y_project/RuoYi) 管理平台框架
